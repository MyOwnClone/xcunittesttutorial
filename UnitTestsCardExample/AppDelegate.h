//
//  AppDelegate.h
//  UnitTestsCardExample
//
//  Created by tomas on 30.05.14.
//  Copyright (c) 2014 myownclone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
