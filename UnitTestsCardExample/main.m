//
//  main.m
//  UnitTestsCardExample
//
//  Created by tomas on 30.05.14.
//  Copyright (c) 2014 myownclone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
